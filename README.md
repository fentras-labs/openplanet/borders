# Borders <sup><sub>Trackmania</sub></sup>
Create virtual borders around the screen to help imitate arbitrary resolutions like `4:3` or reduce distractions by decreasing your field of view.

## Features
* Colors (opacity included)
* Size (adjusts to widescreen automatically)
* Order (overlay under the in-game UI)

## Limitations
* Borders disappear when the game UI is hidden

## Download
* [Openplanet](https://openplanet.dev/plugin/borders)
* [Releases](https://gitlab.com/fentrasLABS/openplanet/borders/-/releases)

## Screenshots
![](_git/1.png)
![](_git/2.png)
![](_git/3.png)

## Credits
*Project icon* by [Fork Awesome](https://forkaweso.me)